package com.first;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

public abstract class Entity {
	private float x;
	private float y;
	private float x_size;
	private float y_size;
	protected float heading;
	Boolean hidden;
	Rectangle around_entity;
	Animation anim;
	MapChunk current_chunk;
	ArrayList<MapChunk> chunkList;
	
	public Entity() {
		this.around_entity= new Rectangle();
	}
	
	public float GetX()
	{	return this.x;	}
	
	public float GetY()
	{  return this.y;	}
	
	public float GetHeading()
	{  return this.heading;	}
	
	public float GetXsize()
	{   return this.x_size;}
	
	public float GetYsize()
	{   return this.y_size;}
	
	
	public void SetX(float x_)
	{	this.x=x_;
		this.around_entity.x=x_;}
//		this.around_entity.x=this.x_size/2*x_;}
	
	public void SetY(float y_)
	{  	this.y=y_;
		this.around_entity.y=y_;}
//		this.around_entity.y=this.y_size/2*y_;}
	
	public void SetHeading(float heading_)
	{  this.heading=heading_;}
	
	public void SetXsize(float x_size_)
	{	this.x_size=x_size_;
		this.around_entity.width=x_size_;}
	
	public void SetYsize(float y_size_)
	{  	this.y_size=y_size_;
		this.around_entity.height=y_size_;}
	
	public void Set_Add_X(float x_)
	{   this.x+=x_;
		this.around_entity.x+=x_;}
	
	public void Set_Add_Y(float y_)
	{   this.y+=y_;
		this.around_entity.y+=y_;}
	
	public void Set_Add_Heading(float heading_)
	{   this.heading+=heading_;}
	
	protected  abstract void DefineTexture();
	
	Boolean checkIntersetion(MapChunk chunk) {
		if(chunk!=null){
			for (Entity ent:chunk.collisionableList){
				if(Intersector.overlaps(ent.around_entity,this.around_entity)){
					return(true);
				}
			}
		}
		return(false);
	}

}
