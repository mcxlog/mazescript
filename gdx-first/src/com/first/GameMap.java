package com.first;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;

public class GameMap {
	private int map_width;
	private int map_height;
	private int chunck_width;
	private int chunck_height;
	private int n_x_chunks;
	private int n_y_chunks;
	
	public ArrayList<MapChunk> chunklist;
	private Generator mapgen;
	private ArrayList<GameMapTile> maparray;
	
	private Texture floor_tiles;
	private Texture wall_tiles;

	
	public GameMap(	int map_width, 
					int map_height,
					int chunck_width,
					int chunck_height,
					OrthographicCamera camera) throws Exception {
		
		this.setMap_width(map_width);
		this.setMap_height(map_height);
		this.setChunck_width(chunck_width);
		this.setChunck_height(chunck_height);
		this.n_x_chunks = map_width/chunck_width;
		this.n_y_chunks = map_height/chunck_height;
		
		if(map_width%chunck_width!=0 || map_height%chunck_height!=0)
				throw new Exception();
		
		chunklist = new  ArrayList<MapChunk>();
		
		floor_tiles = new Texture(Gdx.files.internal("floors.png"));
		wall_tiles = new Texture(Gdx.files.internal("walls.png"));
		
		TextureRegion[][] splitFloorTiles = TextureRegion.split(floor_tiles, 8, 8);
		TextureRegion[][] splitWallTiles  = TextureRegion.split(wall_tiles, 8, 8);

		maparray = new ArrayList<GameMapTile>();
		for (int i=0; i<map_width; i++){
			for(int j=0;j<map_height;j++)
				maparray.add(new GameMapTile(i,j));
		}
		
		mapgen = new Generator(8,8,16,16);	
		mapgen.generate(maparray,this);
		
		for (GameMapTile tile: maparray){
			if(tile.getType()==GameMapTile.TileType.WALL){
			
				int ty = (int)(Math.random() * splitFloorTiles.length);
				int tx = (int)(Math.random() * splitFloorTiles[ty].length);
				tile.setCellTile(new StaticTiledMapTile(splitWallTiles[ty][tx]));
			
			}else if(tile.getType()==GameMapTile.TileType.FLOOR){	
				
				int ty = (int)(Math.random() * splitFloorTiles.length);
				int tx = (int)(Math.random() * splitFloorTiles[ty].length);
				tile.setCellTile(new StaticTiledMapTile(splitFloorTiles[ty][tx]));
			
			}
		}
		
		for (int i=0; i < map_width/chunck_width; i++){
			for (int j=0; j < map_height/chunck_height; j++){
				chunklist.add(new MapChunk(
								maparray,
								map_width,
								map_height,
								chunck_width,
								chunck_height,
								camera, 
								i*chunck_width,
								j*chunck_height
								));
			}
		}
		
		MapChunk chunk;
		for (int i=0; i < n_x_chunks; i++){
			for (int j=0; j < n_y_chunks; j++){
				
				chunk=chunklist.get(i*n_y_chunks+j);
				if(i+1 < n_x_chunks) 
					chunk.chunkright = chunklist.get((i+1)*n_y_chunks+j);
				if(i-1>=0) 
					chunk.chunkleft = chunklist.get((i-1)*n_y_chunks+j);
				if(j+1 < n_y_chunks) 
					chunk.chunkup = chunklist.get(i*n_y_chunks+j+1);
				if(j-1>=0) 
					chunk.chunkdown = chunklist.get(i*n_y_chunks+j-1);
			}
		}
	}


	public int getMap_width() {
		return map_width;
	}


	public void setMap_width(int map_width) {
		this.map_width = map_width;
	}


	public int getMap_height() {
		return map_height;
	}


	public void setMap_height(int map_height) {
		this.map_height = map_height;
	}


	public int getChunck_width() {
		return chunck_width;
	}


	public void setChunck_width(int chunck_width) {
		this.chunck_width = chunck_width;
	}


	public int getChunck_height() {
		return chunck_height;
	}


	public void setChunck_height(int chunck_height) {
		this.chunck_height = chunck_height;
	}
	
}
