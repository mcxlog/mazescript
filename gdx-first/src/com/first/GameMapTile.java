package com.first;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;

public class GameMapTile {

	public enum TileType{
		WALL, 
		FLOOR
	}
	
	private int x;
	private int y;
	private TileType type;
	private Cell cell;
	
	public GameMapTile(int x, int y) {
		this.setX(x);
		this.setY(y);
		cell = new Cell();
	}

	public TileType getType() {
		return type;
	}

	public void setType(TileType type) {
		this.type = type;
	}

	public void setCellTile(StaticTiledMapTile staticTiledMapTile) {
		cell.setTile(staticTiledMapTile);
	}
	
	public Cell getCell(){
		return cell;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
