package com.first;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;

public class Goblin extends Entity {
	int health;
	int experience;
	int attack;
	int defense;
	int stamina;
	float speed;
	public Texture texture;
	public TextureRegion[] regions;
	public Animation stand,jump,walk;
	
	public Goblin(int health, int experience, int attack, int defense, int stamina, float speed, float x, float y, float heading, float x_size, float y_size, ArrayList<MapChunk> chunkList) {
		super();
		this.health = health;
		this.experience = experience;
		this.attack = attack;
		this.defense = defense;
		this.stamina = stamina;
		this.speed = speed;
	//	this.x=	10.0f + (int)(Math.random() * ((5 - 0) + 1));
	//	this.y= 10.0f + (int)(Math.random() * ((5 - 0) + 1));
		this.SetX(x);
		this.SetY(y);
		this.SetHeading(heading);
		this.SetXsize(x_size);
		this.SetYsize(y_size);
		this.hidden=false;
		this.chunkList = chunkList;
		this.current_chunk = this.chunkList.get(((int)x)/MapChunk.chunkWidth*((int)Math.sqrt(this.chunkList.size()))+((int)y)/MapChunk.chunkHeight);
		this.current_chunk.collisionableList.add(this);
	}
	@Override
	public void DefineTexture(){
		this.texture = new Texture("goblin.png"); 
		
		this.regions = TextureRegion.split(this.texture, 78, 118)[0];
		this.stand = new Animation(0, this.regions[2]);
		this.jump = new Animation(0, this.regions[2]);
		this.walk = new Animation(0.15f, this.regions[0],this.regions[1], this.regions[2], this.regions[3],this.regions[4]);
		this.walk.setPlayMode(Animation.LOOP);
	}
	
	public void move(float player_x, float player_y){
		float old_x = this.GetX();
		float old_y = this.GetY();
		
		this.anim=walk;
		if ( (Math.pow((player_y-this.GetY()),2) + Math.pow((player_x-this.GetX()),2) ) < 80) {
			this.pursuitplayer(player_x,player_y);
		}else{
			this.randomWalk();
		}	
		
		Boolean collides=false;
		for (Entity ent:current_chunk.collisionableList){
			if(ent!=this){
				if(Intersector.overlaps(ent.around_entity,this.around_entity)){
					collides=true;
					break;
				}
			}
		}
		
		//Check the multiple possible moves in order to verify the neighbour chunks collisionable lists.
		if(this.GetX()-old_x>0 && collides==false){ //right
			collides=this.checkIntersetion(this.current_chunk.chunkright);
		}
		if(this.GetX()-old_x<0 && collides==false){ //left
			collides=this.checkIntersetion(this.current_chunk.chunkleft);
		}
		if(this.GetY()-old_y>0 && collides==false){ //up
			collides=this.checkIntersetion(this.current_chunk.chunkup);

		}
		if(this.GetY()-old_y<0 && collides==false){ //down
			collides=this.checkIntersetion(this.current_chunk.chunkdown);
		}
		if(this.GetX()-old_x>0 && this.GetY()-old_y>0 && collides==false){ //right up
			if(this.current_chunk.chunkright!=null)
				collides=this.checkIntersetion(this.current_chunk.chunkright.chunkup);
		}
		if(this.GetX()-old_x<0 && this.GetY()-old_y>0 && collides==false){ //left up
			if(this.current_chunk.chunkleft!=null)
				collides=this.checkIntersetion(this.current_chunk.chunkleft.chunkup);
		}
		if(this.GetX()-old_x>0 && this.GetY()-old_y<0 && collides==false){ //right down
			if(this.current_chunk.chunkright!=null)
				collides=this.checkIntersetion(this.current_chunk.chunkright.chunkdown);
		}
		if(this.GetX()-old_x<0 && this.GetY()-old_y<0 && collides==false){ //left down
			if(this.current_chunk.chunkleft!=null)
				collides=this.checkIntersetion(this.current_chunk.chunkleft.chunkdown);
		}
	
		if(!collides){ //check chunk handover
			MapChunk newchunk;
			newchunk = this.chunkList.get(((int)this.GetX())/MapChunk.chunkWidth*((int)Math.sqrt(this.chunkList.size()))+((int)this.GetY())/MapChunk.chunkHeight);
			if(newchunk!=this.current_chunk){
				this.current_chunk.collisionableList.remove(this);
				newchunk.collisionableList.add(this);
				this.current_chunk=newchunk;
			}
		}else{ //revert operation
			this.SetX(old_x);
			this.SetY(old_y);
			this.anim=stand;
		}
		
	}	
	
	public void randomWalk(){
		this.Set_Add_Heading((float) ( -10*2*Math.PI/360 + (int)(Math.random() * ((20*2*Math.PI/360 - (-10*2*Math.PI/360)) + 1)) ) );
		this.Set_Add_X((float) (Math.cos(this.heading)*speed));
		this.Set_Add_Y((float) (Math.sin(this.heading)*speed));
	}
	public void pursuitplayer(float player_x, float player_y){
		this.SetHeading((float) (  Math.atan2(player_y-this.GetY(), player_x-this.GetX()) ) );
		this.Set_Add_X((float) (Math.cos(this.heading)*speed));
		this.Set_Add_Y((float) (Math.sin(this.heading)*speed));
	}
}
