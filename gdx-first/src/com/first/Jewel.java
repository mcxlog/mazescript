package com.first;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Jewel  extends Entity{
	int weight;
	int monetary_value;
	Entity owner;
	public Texture texture;
	public TextureRegion[] regions;
	
	int color;  //use this to in accordance with the image selected (probably not needed)
	
	Jewel( int weight_value, int money_value, float x, float y, float x_size, float y_size){
		super();
		this.weight=weight_value;
		this.monetary_value=money_value;		
		this.hidden=false;
		this.SetX(x);
		this.SetY(y);
		this.SetXsize(x_size);
		this.SetYsize(y_size);
	}

	@Override
	public float GetX()
	{	
		if (!this.hidden)
			return super.GetX();	
		else
			return this.owner.GetX();
	}
	@Override
	public float GetY()
	{  
		if (!this.hidden)
			return super.GetY();	
		else
			return this.owner.GetY();
	}
	
	
	@Override
	public void DefineTexture(){	
		this.texture = new Texture("jewls.png"); 
		
		this.regions = TextureRegion.split(this.texture, 44, 32)[0];
		this.anim = new Animation(0.15f, this.regions[1], this.regions[2], this.regions[3]);
		this.anim.setPlayMode(Animation.LOOP);
	}
	//public static void main(String[] args) {
	// TODO Auto-generated method stub

	//}
}
