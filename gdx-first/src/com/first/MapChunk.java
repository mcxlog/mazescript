package com.first;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
//import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;

public class MapChunk {
	
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera camera;
	public ArrayList<Entity> collisionableList;
	public MapChunk chunkleft =null, chunkright=null, chunkdown=null, chunkup=null;
	static public int chunkWidth;
	static public int chunkHeight;

	public MapChunk	(
					ArrayList<GameMapTile> maparray,
					int map_width,
					int map_height,
					int chunck_width,
					int chunck_height,
					OrthographicCamera camera, 
					int chunk_offset_x,
					int chunk_offset_y
					){
		
		this.camera=camera;		
		MapChunk.chunkWidth = chunck_width;
		MapChunk.chunkHeight = chunck_height;
		collisionableList = new ArrayList<Entity>();

		TiledMap map = new TiledMap();
		MapLayers layers = map.getLayers();

		TiledMapTileLayer layer = new TiledMapTileLayer(map_width, map_height, 8, 8);
		
		for(GameMapTile tile:maparray){	
			if(tile.getX()>=chunk_offset_x && tile.getX()<chunk_offset_x+chunck_width && tile.getY()>=chunk_offset_y && tile.getY()<chunk_offset_y+chunck_width){
				layer.setCell(tile.getX(),tile.getY(), tile.getCell());
				if(tile.getType()==GameMapTile.TileType.WALL){
					collisionableList.add(new MapBlock(tile.getX(), tile.getY(), 1.0f, 1.0f));
				}
			}
		}
		
					
		layers.add(layer);
		renderer=new OrthogonalTiledMapRenderer(map, 1/8f);

	}
	
	public void render(){
		renderer.setView(camera);
		renderer.render();
	}
}
