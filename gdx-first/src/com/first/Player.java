package com.first;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;


public class Player extends Entity{
	int health;
	int experience;
	int attack;
	int defense;
	int stamina;
	int limit_bag_weight;
	int bag_weigth;
	ArrayList<Entity> PlayerItems;
	float speed;
	public Texture texture;
	
	private Animation standanim,standD,standL,standR,standU,jump,walkD,walkR,walkL,walkU;
	
	
	public Player(int health, int experience, int attack, int defense, int stamina, int limit_bag_weight, float speed, float x, float y, float heading, float x_size, float y_size, ArrayList<MapChunk> chunkList) {
		super();
		this.health = health;
		this.experience = experience;
		this.attack = attack;
		this.defense = defense;
		this.stamina = stamina;
		this.limit_bag_weight=limit_bag_weight;
		this.bag_weigth = 0;
		this.speed = speed;
		this.SetX(x);
		this.SetY(y);
		this.SetHeading(heading);
		this.SetXsize(x_size);
		this.SetYsize(y_size);
		this.hidden=false;
		this.PlayerItems = new ArrayList<Entity>();
		this.chunkList = chunkList;
		
		this.current_chunk = this.chunkList.get(((int)x)/MapChunk.chunkWidth*((int)Math.sqrt(this.chunkList.size()))+
												((int)y)/MapChunk.chunkHeight);
		this.current_chunk.collisionableList.add(this);
	}

	@Override
	public void DefineTexture(){
		this.texture = new Texture("player2.png"); 
		
		TextureRegion[] regionsD = TextureRegion.split(this.texture, 32, 32)[0];
		TextureRegion[] regionsL = TextureRegion.split(this.texture, 32, 32)[1];
		TextureRegion[] regionsR = TextureRegion.split(this.texture, 32, 32)[2];
		TextureRegion[] regionsU = TextureRegion.split(this.texture, 32, 32)[3];

		
		
		this.standD = new Animation(0, regionsD[1]);
		this.standL = new Animation(0, regionsR[1]);
		this.standR = new Animation(0, regionsL[1]);
		this.standU = new Animation(0, regionsU[1]);

		
		this.walkD = new Animation(0.15f, regionsD[0], regionsD[1], regionsD[2]);
		this.walkL = new Animation(0.15f, regionsR[0], regionsR[1], regionsR[2]);
		this.walkR = new Animation(0.15f, regionsL[0], regionsL[1], regionsL[2]);
		this.walkU = new Animation(0.15f, regionsU[0], regionsU[1], regionsU[2]);

		this.walkD.setPlayMode(Animation.LOOP);
		this.walkL.setPlayMode(Animation.LOOP);
		this.walkR.setPlayMode(Animation.LOOP);
		this.walkU.setPlayMode(Animation.LOOP);

		this.standanim=this.standD;
	}
	
	public void PickUpJewel(Jewel gotcha){
		if (this.bag_weigth+gotcha.weight<=this.limit_bag_weight){
			this.bag_weigth+=gotcha.weight;		
			this.PlayerItems.add(gotcha);
			gotcha.hidden=true;
			gotcha.owner=this;
		}
	}
	
	public void move(){
		float old_x = this.GetX();
		float old_y = this.GetY();
		
		this.anim=this.standanim;
		
		if(Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.UP)  ) {
			this.Set_Add_Y(speed);	
			this.anim=this.walkU;
			this.standanim=this.standU;
		}

		if(Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT)) {
			this.Set_Add_X(speed);
			this.anim=this.walkL;
			this.standanim=this.standL;
		}
		if(Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.LEFT) ) {
			this.Set_Add_X(-speed);
			this.anim=this.walkR;
			this.standanim=this.standR;
		}
		if(Gdx.input.isKeyPressed(Keys.S) || Gdx.input.isKeyPressed(Keys.DOWN) ) {
			this.Set_Add_Y(-speed);
			this.anim=this.walkD;
			this.standanim=this.standD;
		}
		
		Boolean collides=false;
		for (Entity ent:current_chunk.collisionableList){
			if(ent!=this){
				if(Intersector.overlaps(ent.around_entity,this.around_entity)){
					collides=true;
					break;
				}
			}
		}
		
		//Check the multiple possible moves in order to verify the neighbour chunks collisionable lists.
		if(this.GetX()-old_x>0 && collides==false){ //right
			collides=this.checkIntersetion(this.current_chunk.chunkright);
		}
		if(this.GetX()-old_x<0 && collides==false){ //left
			collides=this.checkIntersetion(this.current_chunk.chunkleft);
		}
		if(this.GetY()-old_y>0 && collides==false){ //up
			collides=this.checkIntersetion(this.current_chunk.chunkup);

		}
		if(this.GetY()-old_y<0 && collides==false){ //down
			collides=this.checkIntersetion(this.current_chunk.chunkdown);
		}
		if(this.GetX()-old_x>0 && this.GetY()-old_y>0 && collides==false){ //right up
			if(this.current_chunk.chunkright!=null)
				collides=this.checkIntersetion(this.current_chunk.chunkright.chunkup);
		}
		if(this.GetX()-old_x<0 && this.GetY()-old_y>0 && collides==false){ //left up
			if(this.current_chunk.chunkleft!=null)
				collides=this.checkIntersetion(this.current_chunk.chunkleft.chunkup);
		}
		if(this.GetX()-old_x>0 && this.GetY()-old_y<0 && collides==false){ //right down
			if(this.current_chunk.chunkright!=null)
				collides=this.checkIntersetion(this.current_chunk.chunkright.chunkdown);
		}
		if(this.GetX()-old_x<0 && this.GetY()-old_y<0 && collides==false){ //left down
			if(this.current_chunk.chunkleft!=null)
				collides=this.checkIntersetion(this.current_chunk.chunkleft.chunkdown);
		}
	
		if(!collides){ //check chunk handover
			MapChunk newchunk;
			newchunk = this.chunkList.get(((int)this.GetX())/MapChunk.chunkWidth*((int)Math.sqrt(this.chunkList.size()))+((int)this.GetY())/MapChunk.chunkHeight);
			if(newchunk!=this.current_chunk){
				this.current_chunk.collisionableList.remove(this);
				newchunk.collisionableList.add(this);
				this.current_chunk=newchunk;
			}
		}else{ //revert operation
			this.SetX(old_x);
			this.SetY(old_y);
		}
	}
}
